# IDB 3 website
**Members**

**Kyle Colburn**
- kjc3328
- @kcolburn38

**Andrew Deng**
- aad2965
- @AndrewElvisDeng

**Franke Tang**
- ft4282
- @ftang21

**Alison Cheung**
- alc4953
- @alch021

**Trung Trinh**
- tnt2288
- @EatWorkSleep

**Alex Wu**
- aw345755
- @TheNecroreaper

**Git SHA**
- dd72aada14395ee9cfa280db5a27b7c8506829a9

**project leader**
Kyle Colburn

**link to GitLab pipelines**
[Pipelines](https://gitlab.com/kcolburn38/idb-3-website/-/pipelines)

**link to website**
[econyoom.me](https://www.econyoom.me/)

**Estimated Completion Time for Each Member: In hours**

- Kyle Colburn: 14
- Andrew Deng: 21
- Franke Tang: 10
- Alison Cheung: 25
- Trung Trinh: 10
- Alex Wu: 20

**Actual Completion Time for each Member: In hours**

- Kyle Colburn: 22 
- Andrew Deng: 26 
- Franke Tang: 17
- Alison Cheung: 26
- Trung Trinh: 22
- Alex Wu: 23

**Comments**